require 'cartesian_grid'
require 'room'

class Dungeon

  attr_reader :rooms
  attr_accessor :map

  # Cardinality: North, South, East, West
  DIRECTION = {
    :n  => [ 0, -1],
    :s  => [ 0,  1],
    :e  => [ 1,  0],
    :w  => [-1,  0],
    :ne => [ 1, -1],
    :nw => [-1, -1],
    :se => [ 1,  1],
    :sw => [-1,  1]
    }

  OPPOSITE = {
    :n => :s,
    :s => :n,
    :w => :e,
    :e => :w,
    :nw => :se,
    :ne => :sw,
    :sw => :ne,
    :se => :nw
  }  

  def initialize(width, height)
    @rooms = []
    @map = CartesianGrid.new width, height
  end

  def pick_room(x, y)
    if (room = @map[x][y]).is_a? Room
      return {
        :room => room, 
        :cell => [x-room.x, y-room.y]
        }
    end
  end

  def self.randomize_cardinality
    DIRECTION.values.shuffle
  end  

  def add(room)
    room.walk do |dx, dy|
      @map[room.x+dx][room.y+dy] = room
    end
    @rooms << room
    room.number = @rooms.length
  end

  def find_neighbors(room)
    return {} if room.number.nil? or @rooms.length < room.number-1 or @rooms[room.number-1] != room

    neighbors = {}
    check_directions = { :n => [], :s => [], :e => [], :w => [] }

    room.edges do |dx, dy|
      check_directions[:n] << [dx, dy] if dy == 0
      check_directions[:s] << [dx, dy] if dy == room.height-1
      check_directions[:w] << [dx, dy] if dx == 0
      check_directions[:e] << [dx, dy] if dx == room.width-1
    end

    x = room.x
    y = room.y

    check_directions.each do |direction, cells|
      cells.each do |coords|
        dx = nx = coords[0]
        dy = ny = coords[1]

        case direction
          when :n
            ny-=1
          when :s
            ny+=1
          when :w
            nx-=1
          when :e
            nx+=1
        end

        next unless @map.in_bounds?(x+nx, y+ny)

        candidate = pick_room(x+nx, y+ny)

        next if candidate.nil?
        candidate_room = candidate[:room]
        candidate_cell = candidate[:cell]

        neighbors[candidate_room] = { :direction => direction, :from_cells => [], :to_cells => [] } if neighbors[candidate_room].nil?
        neighbors[candidate_room][:from_cells] << [dx, dy]
        neighbors[candidate_room][:to_cells] << candidate_cell
      end
    end
    
    return neighbors
  end

end
