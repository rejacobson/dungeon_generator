#!/usr/local/bin/ruby

$: << File.dirname(File.expand_path(__FILE__)) 

require 'generator'
require 'dungeon'
require 'room'
require 'printer'

room1 = Room.new(2, 1)
room2 = Room.new(1, 1)
room3 = Room.new(1, 1)

dungeon = Dungeon.new(5, 5)
dungeon.add! room1.position!(1, 2)
dungeon.add! room2.position!(2, 3)
dungeon.add! room3.position!(3, 3)

room1.add_door! room2, :s, [1, 0], [0, 0]
room2.add_door! room3, :e, [0, 0], [0, 0]

=begin
puts g.dungeon.rooms[0].doors.inspect
puts '--------------------------------------'
puts g.dungeon.rooms[1].doors.inspect
=end

Printer.scaled dungeon, 4

