require 'cartesian_grid'

class Generator

  attr_reader :grid, :dungeon, :room_queue
  attr_accessor :build_strategy, :join_strategy

  def initialize(width, height)
    @dungeon = Dungeon.new width, height
    @grid = @dungeon.map

    @build_strategy = Proc.new do |room, x, y, set_cursor|
      coords = self.find_fit(room, x, y)
      set_cursor.call coords[0], coords[1]
      coords
    end
  end

  def build(rooms, cursor_x = nil, cursor_y = nil)
    return false if !@build_strategy.is_a? Proc

    # The cursor is the location on the map where the generator will try to place the door
    cursor_x = (@grid.width/2).floor if cursor_x.nil?
    cursor_y = (@grid.height/2).floor if cursor_y.nil?

    set_cursor_callback = Proc.new do |x, y|
      cursor_x = x
      cursor_y = y
    end

    while room = rooms.shift
      pos = @build_strategy.call room, cursor_x, cursor_y, set_cursor_callback
      place room, pos[0], pos[1]
    end

    # :shrink_wrap returns a hash of the differences in the new map
    # ie. It returns the changes in x/y coordinates, and the changes in width/height
    # :dx, :dy, :dw, :dh
    # These will be negative integers since the new map is smaller than the old one
    dmap = @grid.shrink_wrap
    @dungeon.rooms.each do |room|
      room.position room.x + dmap[:dx], room.y + dmap[:dy]
    end
  end

  def join(room)
    @dungeon.find_neighbors(room).each do |adjacent_room, placement|
      next if adjacent_room.has_door?

      direction  = placement[:direction]
      from_cells = placement[:from_cells]
      to_cells   = placement[:to_cells]

      index = rand(from_cells.length)

      room.add_door(adjacent_room, direction, from_cells[index], to_cells[index])
      join adjacent_room
    end
  end
=begin
  def join(room)
    @join_strategy.call(room, @dungeon.find_neighbors(room), Proc.new { |adjacent_room, placement| 
      direction  = placement[:direction]
      from_cells = placement[:from_cells]
      to_cells   = placement[:to_cells]

      index = rand(from_cells.length)

      room.add_door(adjacent_room, direction, from_cells[index], to_cells[index])
    })
  end
=end
  def join_random(room)
    neighbors = @dungeon.find_neighbors(room)
    return false if neighbors.empty?
    neighbors.each do |adjacent_room, placement|
      next if room.has_door? adjacent_room
      direction  = placement[:direction]
      from_cells = placement[:from_cells]
      to_cells   = placement[:to_cells]
      index = rand(from_cells.length)
      room.add_door(adjacent_room, direction, from_cells[index], to_cells[index])
      break
    end
    
=begin
    variance = Math.sqrt(@dungeon.rooms.size).floor
    num_doors = (@dungeon.rooms.size/2 - variance/2) + rand(variance)
    rooms = []
    num_doors.times do |i|
      rooms << @dungeon.rooms[rand(@dungeon.rooms.size)]
    end
    rooms.each do |room|
      @dungeon.find_neighbors(room).each do |adjacent_room, placement|
        next if room.has_door?(adjacent_room)
        direction  = placement[:direction]
        from_cells = placement[:from_cells]
        to_cells   = placement[:to_cells]

        index = rand(from_cells.length)

        room.add_door(adjacent_room, direction, from_cells[index], to_cells[index])
        break
      end
    end
=end
  end

  def place(room, x, y)
    @dungeon.add room.position(x, y) 
  end

  def find_fit(room, x, y)
    return [x, y] if fits?(room, x, y)
    Dungeon.randomize_cardinality.each do |dir|
      fit_found = slide(dir, x, y) do |nx, ny|
        if fits?(room, nx, ny)
          x, y = nx, ny 
          break true 
        end
      end
      break if fit_found === true
    end
    return [x, y]
  end

  def slide(dir, x, y)
    return false if dir[0] == 0 and dir[1] == 0
    while @grid.in_bounds?(x, y) do
      x += dir[0]
      yield(x, y) if dir[0] != 0 and @grid.in_bounds?(x, y)

      y += dir[1]
      yield(x, y) if dir[1] != 0 and @grid.in_bounds?(x, y)
    end 
  end

  def fits?(room, x, y)
    fit = room.walk do |dx, dy|
      break false unless @grid.in_bounds?(x+dx, y+dy)
      break false if @grid[x+dx][y+dy].is_a? Room
    end
    return !!fit
  end

end
