class CartesianGrid
  
  attr_reader :width, :height, :map

  def initialize(width, height, default=nil)
    @default = default
    self.map = Array.new(width) { Array.new(height, default) }
  end
 
  def [](x)
    @map[x]
  end

  def map=(map)
    @width = map.length
    @height = map[0].length
    @map = map
  end

  def in_bounds?(x, y)
    return false if x < 0 or y < 0 or x >= @width or y >= @height
    true 
  end

  def walk(&block)
    @map.each_index do |x|
      @map[x].each_index do |y|
        yield x, y, @map[x][y]
      end
    end
  end

  def shrink_wrap(&block)
    min = { :x => nil, :y => nil }
    max = { :x => nil, :y => nil }
    walk do |x, y, value|
      next if value == @default

      min[:x] = x if min[:x].nil? or x < min[:x]
      min[:y] = y if min[:y].nil? or y < min[:y]
      max[:x] = x if max[:x].nil? or x > max[:x]
      max[:y] = y if max[:y].nil? or y > max[:y]
    end

    return false if (min.values + max.values).include? nil

    w = max[:x] - min[:x] + 1
    h = max[:y] - min[:y] + 1
    dw = w - @width
    dh = h - @height
    dx = -min[:x]
    dy = -min[:y]

    map = Array.new(w) { Array.new(h, @default) }

    w.times do |x|
      h.times do |y|
        yield x, y, @map[x-dx][y-dy] if block_given?
        map[x][y] = @map[x-dx][y-dy]
      end
    end

    self.map = map

    { :dx => dx, :dy => dy, :dw => dw, :dh => dh }
  end
  
  def print
    puts ''
    @height.times do |y|
      line = ''
      @width.times do |x|
        val = @map[x][y].to_s
        line << (val.empty? ? " " : val)
      end
      puts line
    end    
  end

end
