require 'dungeon'

class Room

  attr_reader :width, :height, :x, :y, :doors
  attr_accessor :number

  def initialize(w, h)
    @width = w
    @height = h
    @doors = {}
  end

  def position(x, y)
    @x = x
    @y = y
    self
  end

  # Adds a data structure representing a door
  # { Room => { :direction => :n, :cell => [0, 0] } }
  def add_door(adjacent_room, dir, from_cell, to_cell)
    @doors[adjacent_room] = { :direction => dir, :cell => from_cell }
    adjacent_room.add_door(self, Dungeon::OPPOSITE[dir], to_cell, from_cell) unless adjacent_room.has_door?(self, Dungeon::OPPOSITE[dir], to_cell)
  end

  def doors_at(x, y)
    @doors.select { |room, placement| placement[:cell] == [x, y] }  
  end

  def has_door?(adjacent_room=nil, direction=nil, cell=nil)
    result = !@doors.empty?
    result &&= @doors.has_key?(adjacent_room) unless adjacent_room.nil?
    result &&= (@doors[adjacent_room][:direction] == direction) unless direction.nil?
    result &&= (@doors[adjacent_room][:cell] == cell) unless cell.nil?
    result
  end

  def walk(&block)
    @width.times do |x|
      @height.times do |y|
        yield x, y
      end
    end
  end

  def edges(&block)
    @width.times do |x|
      @height.times do |y|
        next if x > 0 and x < @width-1 and y > 0 and y < @height-1
        yield x, y
      end
    end
  end 
end
