#!/usr/local/bin/ruby

$: << File.dirname(File.expand_path(__FILE__)) 

require 'generator'
require 'dungeon'
require 'room'
require 'printer'

rooms = []

7.times do |i|
  w = 1+rand(2)
  h = 1+rand(2)
  rooms << Room.new(w, h)
end

g = Generator.new(20, 10)

g.build_strategy = Proc.new do |room, x, y, block|
  coords = g.find_fit(room, x, y)
  #block.call coords[0], coords[1]
  coords
end

g.build rooms

g.join g.dungeon.rooms[0]

count = g.dungeon.rooms.size
(count/2).times do |i|
  room = g.dungeon.rooms[rand(count)]
  g.join_random room
end

=begin
puts g.dungeon.rooms[0].doors.inspect
puts '--------------------------------------'
puts g.dungeon.rooms[1].doors.inspect
=end

Printer.scaled g.dungeon, 7, 4
