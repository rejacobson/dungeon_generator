require 'spec_helper'
require 'generator'
require 'dungeon'
require 'room'

describe Generator do
  before(:each) do
    opts = { :build_strategy => nil, :join_strategy => nil  }
    @generator = Generator.new 5, 5
  end

  describe '#initialize' do
    it 'creates a Dungeon to hold rooms' do
      @generator.dungeon.class.should == Dungeon
    end

    it 'sets up a CartesianGrid for the dungeon to store room data' do
      @generator.dungeon.map.class.should == CartesianGrid
      @generator.grid.should == @generator.dungeon.map
    end

    it '#build_strategy should be assignable' do
      @generator.build_strategy = 'Build Strategy'
    end

    it '#join_strategy should be assignable' do
      @generator.join_strategy = 'Join Strategy'
    end
  end

  describe '#build' do
    before(:each) do
      @room = Room.new(1, 1).position(1, 1)
      @rooms = [
        Room.new(1, 1).position(0, 0),
        Room.new(1, 2).position(0, 0),
        Room.new(2, 1).position(0, 0),
        Room.new(2, 2).position(0, 0)
      ]
      @generator.stub! :join
    end

    it 'should return false if the generator does not have a build strategy' do
      @generator.build_strategy = nil
      @generator.build(@rooms).should == false
    end

    it 'should accept an array of rooms to place and x:y coordinates to place the first room' do
      @generator.build @rooms, 1, 1
    end

    it 'should remove each room as it places it' do
      @generator.build @rooms
      @rooms.length.should == 0
    end

    it 'should execute the build strategy passing in the room, x, y, and a Proc' do
      test_room = nil
      test_x = nil
      test_y = nil
      test_callback = nil

      @generator.build_strategy = Proc.new do |room, x, y, set_cursor_callback|
        test_room = room
        test_x = x
        test_y = y
        test_callback = set_cursor_callback
        [0, 0]
      end
    
      @generator.build [@room]

      test_room.should be_kind_of(Room)
      test_x.should be_kind_of(Integer)
      test_x.should be_kind_of(Integer)
      test_callback.should be_kind_of(Proc)
    end

    it 'should set the new x:y cursor location if the build stragegy runs the callback' do
      test_x = nil
      test_y = nil

      @generator.build_strategy = Proc.new do |room, x, y, set_cursor_callback|
        test_x = x
        test_y = y
        set_cursor_callback.call(8, 9)
        [0, 0]
      end
    
      @generator.build @rooms[0..1], 0, 0

      test_x.should == 8
      test_y.should == 9
      
    end
=begin
    it 'should get a good x:y coordinate for the room so it fits without overlapping other rooms' do
      @room = Room.new(1, 1)
      @generator.should_receive(:find_fit).with(@room, kind_of(Numeric), kind_of(Numeric)).and_return [1, 1]
      @generator.build [@room]
    end
=end
    it 'should call #place for each room to put it on the grid' do
      @generator.should_receive(:place).exactly(4).times
      @generator.build @rooms
    end

    it 'should shrink wrap the grid to fit rooms tightly' do
      @room1 = Room.new(2, 2)
      @room2 = Room.new(1, 1)
      @generator.place @room1, 2, 2
      @generator.place @room2, 2, 1

      @room1.should_receive(:position).with(@room1.x - 2, @room1.y - 1)
      @room2.should_receive(:position).with(@room2.x - 2, @room2.y - 1)
      
      @generator.stub(:place)
      @generator.build [@room]

      @generator.grid[0][1].should == @room1
      @generator.grid[0][0].should == @room2
    end
  end

  describe '#join' do
    before(:each) do
      @room1 = Room.new(1, 1)
      @room2 = Room.new(1, 1)
      @room3 = Room.new(1, 1)

      @generator.place @room1, 1, 1
      @generator.place @room2, 2, 1
      @generator.place @room3, 2, 2
=begin
      @adjacent_rooms1 = { @room2 => { :e, [[0,0]] ] }
      @adjacent_rooms2 = { @room1 => { :w, [[0, 0]] ], @room3 => [ :e, [[0, 0]] ] }
      @adjacent_rooms3 = { @room2 => { :n, [[0, 0]] ] }
=end
    end

    it 'accepts an initial room to start connecting with' do
      @generator.join @room1
    end

    it 'finds the neighboring rooms' do
      adjacent_rooms = mock('Adjacent Rooms Hash')
      adjacent_rooms.stub! :each
      @generator.dungeon.should_receive(:find_neighbors).with(@room1).and_return adjacent_rooms
      @generator.join @room1
    end

    it 'does not create a door if the adjacent door already has a door somewhere' do
      @room2.stub!(:has_door?).and_return(true)
      @room1.should_not_receive(:add_door)
      @generator.join @room1
    end

    it 'connects with a door and recursively calls #join with the latest room it created a door with' do
      @generator.join @room1
      @room1.has_door?(@room2).should == true
      @room2.has_door?(@room3).should == true
      @room2.has_door?(@room1).should == true
      @room3.has_door?(@room2).should == true
      @room1.has_door?(@room3).should == false
    end
  end

  describe '#join_random' do
    before(:each) do
      @room1 = Room.new(1, 1)
      @room2 = Room.new(1, 1)
      @room3 = Room.new(1, 1)
      @orphan_room = Room.new(1, 1)


      @generator.place @room1, 0, 0
      @generator.place @room2, 1, 0
      @generator.place @room3, 0, 1
      @generator.place @orphan_room, 4, 4

      @room1_neighbors = @generator.dungeon.find_neighbors(@room1)
    end

    it 'should accept a room as a parameter' do
      @generator.join_random @room1
    end

    it 'should get the neighboring rooms' do
      @generator.dungeon.should_receive(:find_neighbors).with(@room1).and_return @room1_neighbors
      @generator.join_random @room1
    end

    it 'returns false if no neighbors are found' do
      @generator.join_random(@orphan_room).should == false
    end

    it 'should join the room with the first unconnected neighbor' do
      @room3.add_door(@room1, :n, [0, 0], [0, 0])
      @room1.should_receive(:add_door).with(@room2, :e, [0, 0], [0, 0])
      @generator.join_random @room1
    end
  end

  describe '#place' do
    before(:each) do
      @room = Room.new(1, 2)
    end

    it 'should add the room to the dungeon' do
      @generator.dungeon.should_receive(:add).with @room
      @room.should_receive(:position).with(2, 2).and_return @room
      @generator.place @room, 2, 2
    end
  end

  describe '#find_fit' do
    before(:each) do
      @small = Room.new(1, 1)
      @wide  = Room.new(2, 1)
      @tall  = Room.new(1, 2)
      @big   = Room.new(2, 2)
      @large = Room.new(3, 3)

      @generator.place @small, 2, 2
    end

    it 'should check the current position for a fit' do
      @generator.find_fit(@small, 1, 1).should == [1, 1]
    end

    it 'should slide the room North' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:n]]
      @generator.find_fit(@big, 2, 2).should == [2, 0] 
    end

    it 'should slide the room South' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:s]]
      @generator.find_fit(@big, 2, 2).should == [2, 3] 
    end

    it 'should slide the room East' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:e]]
      @generator.find_fit(@big, 2, 2).should == [3, 2] 
    end

    it 'should slide the room West' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:w]]
      @generator.find_fit(@big, 2, 2).should == [0, 2] 
    end

    it 'should slide the room North-West' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:nw]]
      @generator.find_fit(@big, 2, 2).should == [0, 1] 
    end

    it 'should slide the room North-East' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:ne]]
      @generator.find_fit(@big, 1, 2).should == [3, 1] 
    end

    it 'should slide the room South-West' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:sw]]
      @generator.find_fit(@big, 2, 1).should == [0, 2] 
    end

    it 'should slide the room South-East' do
      Dungeon.should_receive(:randomize_cardinality).and_return [Dungeon::DIRECTION[:se]]
      @generator.find_fit(@big, 1, 1).should == [3, 2] 
    end
  end

  describe '#slide' do
    it 'should return false if the direction to move is invalid. ie. x and y components are both 0' do
      @generator.slide([0, 0], 1, 1).should == false
    end

    def test_slider(dir, expected)
      results = []
      @generator.slide(dir, 2, 2) do |x, y|
        results << [x, y]
      end
      results.should == expected
    end

    it 'slides in each cardinal direction' do
      # North
      test_slider Dungeon::DIRECTION[:n], [[2, 1], [2, 0]]

      # South
      test_slider Dungeon::DIRECTION[:s], [[2, 3], [2, 4]]

      # East
      test_slider Dungeon::DIRECTION[:e], [[3, 2], [4, 2]]

      # West
      test_slider Dungeon::DIRECTION[:w], [[1, 2], [0, 2]]
  
      # NW
      test_slider Dungeon::DIRECTION[:nw], [[1, 2], [1, 1], [0, 1], [0, 0]]

      # NE
      test_slider Dungeon::DIRECTION[:ne], [[3, 2], [3, 1], [4, 1], [4, 0]]

      # SW
      test_slider Dungeon::DIRECTION[:sw], [[1, 2], [1, 3], [0, 3], [0, 4]]

      # SE
      test_slider Dungeon::DIRECTION[:se], [[3, 2], [3, 3], [4, 3], [4, 4]]
    end
  end

  describe '#fits?' do
    before(:each) do
      @small = Room.new(1, 1)
      @wide  = Room.new(2, 1)
      @tall  = Room.new(1, 2)
      @big   = Room.new(2, 2)
      @large = Room.new(3, 3)
    end
    
    it 'should return true if there is no overlap' do
      @generator.place @small, 2, 2

      @generator.fits?(@small, 1, 2).should == true

      @generator.fits?(@wide,  2, 1).should == true
      @generator.fits?(@wide,  0, 2).should == true

      @generator.fits?(@tall,  1, 2).should == true
      @generator.fits?(@tall,  2, 0).should == true
      
      @generator.fits?(@big,  0, 0).should == true
      @generator.fits?(@big,  0, 2).should == true
      @generator.fits?(@big,  2, 0).should == true
      @generator.fits?(@big,  3, 2).should == true 
    end
  
    it 'should return false if the room would overlap another room' do
      @generator.place @small, 2, 2

      @generator.fits?(@small, 2, 2).should == false

      @generator.fits?(@wide,  2, 2).should == false
      @generator.fits?(@wide,  1, 2).should == false

      @generator.fits?(@tall,  2, 2).should == false
      @generator.fits?(@tall,  2, 1).should == false
      
      @generator.fits?(@big,  2, 2).should == false
      @generator.fits?(@big,  1, 2).should == false
      @generator.fits?(@big,  2, 1).should == false
      @generator.fits?(@big,  1, 1).should == false 
    end
  end

end
