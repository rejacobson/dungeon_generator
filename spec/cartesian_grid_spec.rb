require 'spec_helper'
require 'cartesian_grid'

describe CartesianGrid do
  before(:each) do
    @grid = CartesianGrid.new 4, 4, 'a'
  end

  describe '#initialize' do
    it 'initializes the grid data structure as a 2d array' do
      @grid.width.should == 4
      @grid.height.should == 4
      @grid[3].length.should == 4
    end
  end

  describe 'Array access operator' do
    it 'reads the value of the cell at x:y' do
      @grid[0][0].should == 'a'
      @grid[3][3].should == 'a'
    end

    it 'sets the value of the cell at x:y' do
      @grid[0][0] = 'x'
      @grid[0][0].should == 'x'
      @grid[3][3] = 'y'
      @grid[3][3].should == 'y'
    end
  end

  describe '#map=' do
    it 'should set the width and height when setting a new map' do
      map = Array.new(2) { Array.new(2) }
      @grid.map = map
      @grid.width.should == 2
      @grid.height.should == 2
    end
  end

  describe '#walk' do
    it 'loops through each cell in the grid' do
      times = 0
      @grid.walk do |x, y, cell|
        times += 1
      end
      times.should == @grid.width * @grid.height
    end
  end

  describe '#in_bounds?' do
    it 'should return true if coordinates are within the grid boundaries' do
      @grid.in_bounds?(0, 0).should == true
      @grid.in_bounds?(3, 3).should == true
    end

    it 'should return false if the coordinates are outside of the grid boundaries' do
      @grid.in_bounds?(-1, 0).should == false
      @grid.in_bounds?(0, -1).should == false
      @grid.in_bounds?(5, 0).should == false
      @grid.in_bounds?(0, 5).should == false
    end
  end

  describe '#shrink_wrap' do
    it 'return false if the map is empty' do
      @grid.shrink_wrap.should == false
    end

    it 'creates a smaller grid with the original grid padding stripped' do
      @grid[1][1] = 'X'
      @grid[2][2] = 'Y'

      @grid.shrink_wrap

      @grid.width.should == 2
      @grid.height.should == 2
      @grid[0][0].should == 'X'
      @grid[1][1].should == 'Y'
    end

    it 'returns a hash of the changes made' do
      expected = { :dx => -1, :dy => -1, :dw => -2, :dh => -2 }
      @grid[1][1] = 'X'
      @grid[2][2] = 'Y'
      @grid.shrink_wrap.should == expected
    end
  end
end

