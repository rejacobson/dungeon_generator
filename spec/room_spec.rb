require 'spec_helper'
require 'room'

describe Room do
  before(:each) do
    @room = Room.new(2, 2)
  end

  describe '#initialize' do
    it 'should create a new Room object with defaults' do
      @room.width.should == 2
      @room.height.should == 2
      @room.doors.should == {}
    end
  end

  describe '#position' do
    it '#x #y should be available to return coordinates' do
      @room.x.should == nil
      @room.y.should == nil  
    end

    it 'should set the x and y coordinates' do
      @room.position(2, 2)
      @room.x.should == 2
      @room.y.should == 2
    end

    it 'should return the room object' do
      @room.position(2, 2).should == @room
    end
  end

  describe '#walk' do
    it 'should loop through the width and height of the room' do
      count = 0
      @room.walk do |dx, dy|
        count += 1
      end
      count.should == 4
    end
  end

  describe '#edges' do
    it 'should loop through the edge cells of the room, skipping the center cells' do
      room1 = Room.new(2, 2)
      room2 = Room.new(4, 4)

      expected = [[0, 0], [0, 1], [1, 0], [1, 1]]
      result = []
      room1.edges do |dx, dy|
        result << [dx, dy]
      end
      result.should == expected

      expected = [[0, 0], [0, 1], [0, 2], [0, 3], [1, 0], [1, 3], [2, 0], [2, 3], [3, 0], [3, 1], [3, 2], [3, 3]]
      result = []
      room2.edges do |dx, dy|
        result << [dx, dy]
      end
      result.should == expected
    end
  end

  describe '#add_door' do
    it 'adds a door to the passed in adjacent room' do
      new_room = Room.new(1, 1)
      @room.add_door new_room, :n, [3, 0], [2, 1]
      @room.doors.should == { new_room => { :direction => :n, :cell => [3, 0] } }
      new_room.doors.should == { @room => { :direction => :s, :cell => [2, 1] } }
    end
  end

  describe '#doors_at' do
    it 'should return all doors at a specified cell' do
      room1 = Room.new(1, 1)
      room2 = Room.new(1, 1)
      room3 = Room.new(1, 1)

      @room.add_door room1, :s, [1, 1], [0, 0]
      @room.doors_at(1, 1).should == { room1 => @room.doors[room1] }

      @room.add_door room2, :e, [1, 1], [0, 0]
      @room.doors_at(1, 1).should == { room1 => @room.doors[room1], room2 => @room.doors[room2] }

      @room.add_door room3, :n, [1, 0], [0, 0]
      @room.doors_at(1, 1).should == { room1 => @room.doors[room1], room2 => @room.doors[room2] }
    end
  end

  describe '#has_door?' do
    before(:each) do
      @new_room = Room.new(1, 1)
    end

    it 'should return true if the room has any doors' do
      @room.add_door Room.new(1, 1), :n, [0, 0], [0, 0]
      @room.has_door?.should == true
    end

    it 'should return false if the room does not have any doors' do
      @room.has_door?.should == false
    end

    it 'should return true if the room has a door to the adjacent room queried about' do
      @room.add_door @new_room, :n, [0, 0], [0, 0]
      @room.has_door?(@new_room).should == true
    end

    it 'should return false if the room does not have a door to the adjacent room' do
      @room.has_door?(@new_room).should == false
    end

    it 'should return true if the room has a door in a particular direction' do
      @room.add_door @new_room, :n, [0, 0], [0, 0]
      @room.has_door?(@new_room, :n).should == true
    end

    it 'should return false if the room does not have a door in a particular direction' do
      @room.has_door?(@new_room, :e).should == false
      @room.add_door @new_room, :s, [0, 0], [0, 0]
      @room.has_door?(@new_room, :n).should == false
    end

    it 'should return true if the room has a door in a particular direction in a certain cell' do
      @room.add_door @new_room, :n, [1, 1], [0, 0]
      @room.has_door?(@new_room, :n, [1, 1]).should == true
    end

    it 'should return false if the room does not have a door in a particular direction in a certain cell' do
      @room.has_door?(@new_room, :e, [2, 2]).should == false
      @room.add_door @new_room, :n, [0, 0], [0, 0]
      @room.has_door?(@new_room, :n, [1, 1]).should == false
    end
  end

  describe '#has_doors?' do
  end
end
