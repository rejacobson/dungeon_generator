require 'spec_helper'
require 'dungeon'

describe Dungeon do
  before(:each) do
    @dungeon = Dungeon.new 5, 5
    @room = Room.new 1, 1
  end

  describe '#initialize' do
    it 'sets up the dungeon with default values' do
      @dungeon.rooms.should == []
      @dungeon.map.should be_kind_of(CartesianGrid)
      @dungeon.map.width.should == 5
      @dungeon.map.height.should == 5
    end

    it 'should have a list of directions. eg. north, south, east, west' do
      Dungeon::DIRECTION.should == { 
        :n  => [ 0, -1],
        :s  => [ 0,  1],
        :e  => [ 1,  0],
        :w  => [-1,  0],
        :ne => [ 1, -1],
        :nw => [-1, -1],
        :se => [ 1,  1],
        :sw => [-1,  1]
        }
    end

    it 'should have a list of opposite cardinalities' do
      Dungeon::OPPOSITE.should == {
        :n => :s,
        :s => :n,
        :w => :e,
        :e => :w,
        :nw => :se,
        :ne => :sw,
        :sw => :ne,
        :se => :nw
      }
    end
  end

  describe '#randomize_cardinality' do
    it 'should shuffle the directions and return them as an array' do
      shuffled = Dungeon.randomize_cardinality
      shuffled.should be_kind_of(Array)
      shuffled.length.should == 8
      shuffled.each do |a|
        a.length.should == 2
      end
      shuffled.should_not == Dungeon::DIRECTION.values
    end
  end

  describe '#add' do
    it 'should add a room to the rooms list' do
      @room.position(1, 1)
      @dungeon.add @room
      @dungeon.rooms.length.should == 1
      @dungeon.rooms[0].number.should == 1
    end

    it 'should place a Room object on the grid at the x:y coordinates, covering a grid area equal to the area of the room' do
      room = Room.new(2, 2).position(1, 1)
      @dungeon.add room
      @dungeon.map[1][1].should == room
      @dungeon.map[2][1].should == room
      @dungeon.map[1][2].should == room
      @dungeon.map[2][2].should == room
      @dungeon.map[0][0].should_not == room      
    end

  end

  describe '#pick_room' do
    it 'should return nil if there is no room at the position' do
      @dungeon.pick_room(0, 0).should == nil
    end

    it 'should return a room cell position using an absolute grid position' do
      room = Room.new(2, 2).position(2, 2)
      @dungeon.add room
      @dungeon.pick_room(2, 2).should == { :room => room, :cell => [0, 0] }
      @dungeon.pick_room(3, 2).should == { :room => room, :cell => [1, 0] }
      @dungeon.pick_room(2, 3).should == { :room => room, :cell => [0, 1] }
      @dungeon.pick_room(3, 3).should == { :room => room, :cell => [1, 1] }
      @dungeon.pick_room(2, 1).should == nil
      @dungeon.pick_room(1, 2).should == nil
      @dungeon.pick_room(4, 3).should == nil
      @dungeon.pick_room(3, 4).should == nil
    end
  end

  describe '#find_neighbors' do
    #      0  1  2  3  4  5  6  7  8  9  0
    #      _____ __ __
    # 0   |2    |3 |4 |                    0
    #     |     |__|__|_____
    # 1   |     |1    |6    |              1
    #     |_____|     |     |
    # 2         |     |     |              2
    #           |_____|_____|
    # 3            |5 |                    3
    #              |__|
    # 4                                    4
    #
    # 5                                    5
    #
    # 6                                    6
    before(:each) do
      @dungeon = Dungeon.new(10, 10)

      @room1 = Room.new(2, 2).position(2, 2)
      @room2 = Room.new(2, 2).position(0, 1)
      @room3 = Room.new(1, 1).position(2, 1)
      @room4 = Room.new(1, 1).position(3, 1)
      @room5 = Room.new(1, 1).position(3, 4)
      @room6 = Room.new(2, 2).position(4, 2)

      @dungeon.add @room1
      @dungeon.add @room2
      @dungeon.add @room3
      @dungeon.add @room4
      @dungeon.add @room5
      @dungeon.add @room6
    end

    it 'should accept a Room object as a parameter' do
      @dungeon.find_neighbors @room1
    end

    it 'should return an empty array if the room does not exist in the dungeon room list' do
      room = Room.new 2, 2
      @dungeon.find_neighbors(room).should == {}
    end

    it 'should walk through the room area looking for neighboring rooms on each edge cell' do
      expected = {
        @room2 => { :direction => :w, :from_cells => [[0, 0]], :to_cells => [[1, 1]] },
        @room3 => { :direction => :n, :from_cells => [[0, 0]], :to_cells => [[0, 0]] },
        @room4 => { :direction => :n, :from_cells => [[1, 0]], :to_cells => [[0, 0]] },
        @room5 => { :direction => :s, :from_cells => [[1, 1]], :to_cells => [[0, 0]] },
        @room6 => { :direction => :e, :from_cells => [[1, 0], [1, 1]], :to_cells => [[0, 0], [0, 1]] }
      }
      @dungeon.find_neighbors(@room1).should == expected
    end
  end

end
