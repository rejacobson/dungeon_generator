#!ruby19
# encoding: utf-8

require 'colorize'
require 'cartesian_grid'

class Printer

  N_WALL  = 9552.chr(Encoding::UTF_8).blue
  S_WALL  = N_WALL
  W_WALL  = 9553.chr(Encoding::UTF_8).blue
  E_WALL  = W_WALL
  NW_WALL = 9556.chr(Encoding::UTF_8).blue
  NE_WALL = 9559.chr(Encoding::UTF_8).blue
  SW_WALL = 9562.chr(Encoding::UTF_8).blue
  SE_WALL = 9565.chr(Encoding::UTF_8).blue
  FLOOR0  = 8729.chr(Encoding::UTF_8).red
  FLOOR1  = 9643.chr(Encoding::UTF_8) # Small hollow square 
  FLOOR2  = 9688.chr(Encoding::UTF_8) # Small hollow square (bold)
  FLOOR3  = 9702.chr(Encoding::UTF_8) # Small hollow circle
  FLOOR4  = 9617.chr(Encoding::UTF_8) # Dark dither 
  FLOOR5  = 9618.chr(Encoding::UTF_8) # Medium Dither
  FLOOR6  = 9619.chr(Encoding::UTF_8) # Bright Dither
  FLOOR7  = 9608.chr(Encoding::UTF_8) # Solid white
  

  def self.basic(dungeon)
    puts ''
    dungeon.map.height.times do |y|
      line = ''
      dungeon.map.width.times do |x|
        room = dungeon.map[x][y]
        line << (room ? room.number.to_s : '.')
      end
      puts line
    end    
  end

  def self.scaled(dungeon, scalex=2, scaley=2, padding=1)
    scaled_w = (dungeon.map.width) * scalex
    scaled_h = (dungeon.map.height) * scaley
    grid = CartesianGrid.new scaled_w, scaled_h, ' '

    grid.walk do |x, y, v|
      grid[x][y] = FLOOR0
    end

    dungeon.rooms.each do |room|
      scaled_room_w = room.width*scalex
      scaled_room_h = room.height*scaley

      room_grid = self.print_room room, scaled_room_w, scaled_room_h

      x = room.x * scalex
      y = room.y * scaley

      room_grid.each_index do |rx|
        room_grid[rx].each_index do |ry|
          grid[x+rx][y+ry] = room_grid[rx][ry]
        end
      end
    end

    grid.print
  end

  def self.print_room(room, width, height)
    room_grid = Array.new(width) { Array.new(height) }

    # Place floors
    if room.number == 1
      (width-2).times do |x|
        (height-2).times do |y|
          room_grid[x+1][y+1] = FLOOR0.light_blue #9617  9688  9689  9702  9618  9619
          room_grid[x+1][y+1]
        end
      end
    end

    # Place corners
    room_grid[0][0]              = NW_WALL
    room_grid[width-1][0]        = NE_WALL
    room_grid[0][height-1]       = SW_WALL
    room_grid[width-1][height-1] = SE_WALL

    # Place top and bottom walls
    (width-2).times do |x|
      room_grid[x+1][0]        = N_WALL if room_grid[x+1][0].nil?
      room_grid[x+1][height-1] = S_WALL if room_grid[x+1][height-1].nil?
    end
    
    # Place left and right walls
    (height-2).times do |y|
      room_grid[0][y+1]       = W_WALL if room_grid[0][y+1].nil?
      room_grid[width-1][y+1] = E_WALL if room_grid[width-1][y+1].nil?
    end
   
    # Place doors
    if room.has_door?
      scalex = width/room.width
      scaley = height/room.height

      room.doors.each do |adjacent_room, placement|
        direction = placement[:direction]
        cell = placement[:cell]

        x = cell[0]*scalex + 1 #(scalex/2).floor - 1
        y = cell[1]*scaley + 1 #(scaley/2).floor - 1

        case direction
          when :n
            room_grid[x][0]   = (x==0 ? W_WALL : SE_WALL) 
            room_grid[x+1][0] = (x==width-2 ? E_WALL : SW_WALL)

          when :s
            room_grid[x][height-1] = (x==0 ? W_WALL : NE_WALL)
            room_grid[x+1][height-1] = (x==width-2 ? E_WALL : NW_WALL)
            
          when :w
            room_grid[0][y] = (y==0 ? N_WALL : SE_WALL)
            room_grid[0][y+1] = (y==height-2 ? S_WALL : NE_WALL)
            
          when :e
            room_grid[width-1][y] = (y==0 ? N_WALL : SW_WALL)
            room_grid[width-1][y+1] = (y==height-2 ? S_WALL : NW_WALL)
            
        end
      end
    end
 
    return room_grid 
  end
end
